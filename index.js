const changeBgBtn = document.querySelectorAll('.change-color-btn')
const body = document.querySelector('body')
loder()
for (i = 1; i < changeBgBtn.length; i++) {
  changeBgBtn[i].addEventListener('click', (e) => {
    loder()
  })
}

function loder() {
  const values = [
    '0',
    '1',
    '2',
    '3',
    '4',
    '5',
    '6',
    '7',
    '8',
    '9',
    'a',
    'b',
    'c',
    'd',
    'e',
    'f',
  ]
  const mixer = () => {
    let adder = ''
    for (let i = 0; i < 6; i++) {
      let random = Math.round(Math.random() * 15)
      let valueRandom = values[random]
      adder += valueRandom
    }
    return adder
  }

  const mixer1 = mixer()
  const mixer2 = mixer()
  const randomDeg = Math.floor(Math.random() * 361)
  const linearGrad = `linear-gradient(${randomDeg}deg, #${mixer1} , #${mixer2})`
  body.style.background = linearGrad
  document.querySelector('.color-text').textContent = linearGrad
}
